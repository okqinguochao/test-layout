const path = require('path');
const { defineConfig } = require('@vue/cli-service')
const resolve = dir => path.join(__dirname, './', dir);
module.exports = defineConfig({
  lintOnSave:false,
  transpileDependencies: true
})
module.exports = {
  css: {
  loaderOptions: {
      sass: {
      // 具体路径根据项目来
          additionalData: `@import "@/assets/theme.scss";`
      },
      scss: {
          additionalData: `@import "@/assets/theme.scss";`
      }
  }
},
chainWebpack: (config) => {
  // 移除 prefetch 插件
  config.plugins.delete('prefetch');

  // 批量处理 svg
  config.module.rule('svg').exclude.add(resolve('src/icons/svg')).end();//svg所在的目录文件名字
  config.module
    .rule('icons')
    .test(/\.svg$/)
    .include.add(resolve('src/icons/svg'))
    .end()
    .use('svg-sprite-loader')
    .loader('svg-sprite-loader')
    .options({
      symbolId: 'icon-[name]',
    });
},
}