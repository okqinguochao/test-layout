import { createStore } from 'vuex'

export default createStore({
  state: {
    isWrapMenu:false,
  },
  getters: {
  },
  mutations: {
    wrapMenu(state, payload){
      state.isWrapMenu = payload
    }
  },
  actions: {
  },
  modules: {
  }
})
