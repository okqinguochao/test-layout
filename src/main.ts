import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'


import "@/icons/index"
import svgIcon from "@/icons/icon-svg.vue";

createApp(App).use(store).use(router).use(ElementPlus).component('icon-svg',svgIcon).mount('#app')
