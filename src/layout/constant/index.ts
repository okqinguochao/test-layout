// // 菜单参数
// export interface MenuItem {
//     childList: MenuItem[],
//     ordinal:string,
//     parentResourceId:string,
//     resourceCode:string,
//     resourceId: string,
//     resourceName:string,
//     resourceType:string,
//     resourceUrl:string,
//     systemType:string,
// }
// 菜单列表
export interface MenuList {
    childList:MenuList[],
    ordinal:string,
    parentResourceId:string,
    resourceCode:string,
    resourceId: string,
    resourceName:string,
    resourceType:string,
    resourceUrl:string,
    systemType:string,
}